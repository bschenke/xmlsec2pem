Quite surprisingly it turned out out lately that I cannot find any utility
that would help me convert a private RSA key in the XML format to the usual PEM format.
The XML format is mostly used for digitally signing XML documents according
the the W3C recommendation, but Microsoft jumped on this format, too,
and uses it in its .NET 1.1 platform. It is thus quite probable that, time-to-time,
someone needs to convert this XML “beast” back to normal PEM format.
But it seems that no suitable utility is to be found on the whole internet!

http://www.platanus.cz/blog/converting-rsa-xml-key-to-pem